<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
         "post_title"=>$faker->title;
         "post_body"=>$faker->paragraph
    ];
});
