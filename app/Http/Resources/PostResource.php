<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class PostResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
            return [
                 
                'post_title' => $this->post_title,
                'post_body' => $this->post_body,
                'author' => $this->user_name,
                'likes' => $this->likes,
                'dislikes' => $this->dislikes,
                'post image' => $this->post_image,
                'category_id' => $this->category_id,
                'post views' => $this->views,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                // 'view_post' => route('post_view',$this->id),
                // 'post_edit' => route('post_edit',$this->id)
                 
                'links'=>[
                // "view post" =>route('posts.show',$this->id),
                 'like post' =>route('like',$this->id),
                 'dislike post' =>route('dislike',$this->id),
                 'dislike post' => route('dislike',$this->id),
                 'Update post'=>route('posts.update',$this->id ),
                 'delete post'=>route('posts.destroy',$this->id ),
                
                ]
                
                
               ];

           }


           
           
        // private function links($post)
        // {
        //    return [
        //                      'post_update' => $this->when(checkUser($post), [
        //                        'view_post' => route('post_view',$this->id),
        //                         'post_edit' => route('post_edit',$this->id) 

        //                      ])
        //                  ];
        // }
}




